FROM debian:stable

ENV INSTALL_DIR=/install-tl \
    INSTALL_REPOSITORY=ftp://ftp.u-aizu.ac.jp/pub/tex/CTAN/systems/texlive/tlnet/
ENV TLNET_REPOSITORY=
ENV TERM=dumb

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    locales wget perl \
    python-setuptools ghostscript \
 && update-locale LANG=C.UTF-8 LC_MESSAGES=POSIX \
 && locale-gen en_US.UTF-8 \
 && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales \
 && rm -rf /var/lib/apt/lists/*

RUN wget -O archive-install-tl.tar.gz "${INSTALL_REPOSITORY}/install-tl-unx.tar.gz" \
 && tar xf archive-install-tl.tar.gz \
 && mkdir -p $(dirname "${INSTALL_DIR}") \
 && mv install-tl-* "${INSTALL_DIR}" \
 && rm -rf archive-install-tl.tar.gz

RUN cd "${INSTALL_DIR}" \
 && echo I | ./install-tl -no-gui -scheme scheme-small \
   $(if [ -z "${TLNET_REPOSITORY}" ]; then echo ""; else echo "-repository=${TLNET_REPOSITORY}"; fi) \
 && cd .. \
 && rm -rf "${INSTALL_DIR}" \
 && ln -sf $(dirname /usr/local/texlive/*/bin/*/tlmgr) /usr/local/texlive/texbin

ENV PATH="/usr/local/texlive/texbin:${PATH}"

# for tlmgr bugs...
RUN tlmgr init-usertree \
 && tlmgr install collection-langjapanese

RUN easy_install Pygments

CMD ["platex"]

