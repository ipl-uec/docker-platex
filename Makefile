.PHONY: all
all: build

.PHONY: build
build: Dockerfile
	docker build -t ipl-uec/docker-platex .
